package com.packenius.fid.gui;

import java.awt.GraphicsEnvironment;

import javax.swing.JPanel;

/**
 * @author Christian Packenius, 2019.
 */
public class JPanelWithDesiredFont extends JPanel {
    private static final long serialVersionUID = -705719795444880414L;

    /**
     * Get best (last) font that is installed here.
     */
    public static String getDesiredFont(String... desiredFonts) {
        for (int i = desiredFonts.length - 1; i >= 0; i--) {
            String desiredFont = desiredFonts[i];
            if (isFontAvailable(desiredFont)) {
                return desiredFont;
            }
        }
        return null;
    }

    /**
     * Checks if the given font is available.
     */
    public static boolean isFontAvailable(String desiredFontFamily) {
        for (String fontFamily : GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames()) {
            if (fontFamily.equals(desiredFontFamily)) {
                return true;
            }
        }
        return false;
    }
}
