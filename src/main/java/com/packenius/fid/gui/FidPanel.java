package com.packenius.fid.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.packenius.datadivider.DataDivider;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.fid.gui.label.AddressedLabel;

/**
 * Single panel showing a diagram.
 * @author Christian Packenius, 2019.
 */
public class FidPanel extends JPanelWithDesiredFont
        implements MouseMotionListener, MouseListener, ContainerListener, MouseWheelListener, KeyListener {
    private static final String FID_TITLE = "FiD - The File Diver";

    private static final long serialVersionUID = 8405280058584191830L;

    private static final Color DARK_RED = new Color(64, 0, 0);

    private static final Color DARK_BLUE = new Color(0, 0, 64);

    private static final Color DARK_GREEN = new Color(0, 64, 0);

    private final List<Arrow> arrows = new ArrayList<>();

    public boolean drawAuthor = true;

    /**
     * Arrow color.
     */
    public static final Color arrowColor = new Color(0xffd0d0d0);

    private DumpTextBox mainBox;

    /**
     * Position the mouse pointer was last time in pressed state.
     */
    private Point lastPressedMousePosition;

    /**
     * Label that is currently designed as having focus (or as having mouse pointer
     * on its area). We call it "choosen".
     */
    private AddressedLabel choosenAddressedLabel;

    /**
     * FileChooser for choosing class to open.
     */
    private final JFileChooser fc = new JFileChooser();

    /**
     * This DataDivider object is used when opening new content (byte array).
     */
    private DataDivider dataDivider;

    // Register Noto fonts.
    static {
        registerFont("NotoEmoji-Regular.ttf", "Noto Emoji");
        registerFont("NotoMono-Regular.ttf", "Noto Mono");
    }

    private static void registerFont(String filename, String fontFamily) {
        if (!isFontAvailable(fontFamily)) {
            try (InputStream in = FidPanel.class.getResourceAsStream(filename)) {
                Font font = Font.createFont(Font.TRUETYPE_FONT, in);
                GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(font);
            } catch (Exception e) {
                // If this is a problem, we already can live without.
                // -> There are some fallbacks within the program. ;-)
                // (But we will have no emojis.)
            }
        }
    }

    /**
     * Constructor with default DataDivider usage.
     */
    public FidPanel(DumpBlock dumpBlock) {
        this(dumpBlock, null);
    }

    /**
     * Constructor.
     */
    public FidPanel(DumpBlock dumpBlock, DataDivider dataDivider) {
        initPanel();
        setDataDivider(dataDivider);
        setMainDumpBlock(dumpBlock);
    }

    private void setDataDivider(DataDivider dataDivider) {
        this.dataDivider = dataDivider == null ? new DataDivider() : dataDivider;
    }

    private void initPanel() {
        setDoubleBuffered(true);
        setLayout(null);
        setFocusable(true);

        addKeyListener(this);
        addMouseListener(this);
        addMouseMotionListener(this);
        addMouseWheelListener(this);
        addContainerListener(this);
    }

    private final static String[] desiredFonts = { "Consolas", "Lucida Console", "Noto Mono" };

    @Override
    protected void paintComponent(Graphics gr) {
        Graphics2D g = (Graphics2D) gr;
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        int width = getWidth();
        int height = getHeight();

        // Remove background with gradient color.
        g.setPaint(new GradientPaint(0, 0, DARK_BLUE, width, height, DARK_RED));
        g.fillRect(0, 0, width, height);

        // Draw arrows and monkeys.
        drawArrows(g);
        int xx = drawThreeMonkeys(g);

        // Write name of the tool.
        String font = getDesiredFont(desiredFonts);
        if (font != null) {
            g.setPaint(new GradientPaint(0, height, Color.DARK_GRAY, width, 0, Color.WHITE, true));
            int min = Math.min(width / 15, height / 15);
            g.setFont(new Font(font, Font.BOLD, min));
            int yy = height - xx * 5 / 2;
            g.drawString(FID_TITLE, xx, yy);
            int ftLength = (int) (g.getFontMetrics().getStringBounds(FID_TITLE, g).getWidth());
            g.setFont(new Font(font, Font.PLAIN, min / 3));
            g.setColor(DARK_GREEN);
            if (drawAuthor) {
                g.drawString(" (w) by christian packenius, 2019", xx + ftLength, yy);
            } else {
                g.drawString(" (w) 2019", xx + ftLength, yy);
            }
        }
    }

    private void drawArrows(Graphics2D g) {
        g.setColor(arrowColor);
        for (Arrow arrow : arrows) {
            int x1 = arrow.x1;
            int x2 = arrow.x2;
            int x3 = arrow.x3;
            int y1 = arrow.y1;
            int y2 = arrow.y2;
            if (arrow.dottedArrow) {
                g.setStroke(
                        new BasicStroke(3, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 0, new float[] { 8, 8 }, 0));
            } else {
                g.setStroke(new BasicStroke(3, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND));
            }
            g.drawLine(x1, y1, x2, y1);
            g.drawLine(x2, y1, x2, y2);
            g.drawLine(x2, y2, x3, y2);
            g.setStroke(new BasicStroke(3, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND));
            g.drawLine(x3 - 15, y2 - 10, x3, y2);
            g.drawLine(x3 - 15, y2 + 10, x3, y2);
        }
    }

    private int drawThreeMonkeys(Graphics2D g) {
        int corner = 64;
        String desiredFont = getDesiredFont("Noto Emoji");
        if (desiredFont != null) {
            g.setFont(new Font(desiredFont, Font.PLAIN, corner));
            String dreiAffen = new String(new int[] { 0x1F648, 0x1F649, 0x1F64A }, 0, 3);
            String affe1 = dreiAffen.substring(0, 2);
            String affe2 = dreiAffen.substring(2, 4);
            String affe3 = dreiAffen.substring(4, 6);
            FontMetrics fontMetrics = g.getFontMetrics();
            int lenA1 = fontMetrics.stringWidth(affe1);
            int lenA2 = fontMetrics.stringWidth(affe2);
            g.setColor(new Color(96, 192, 96));
            g.drawString(affe1, corner, getHeight() - corner);
            g.setColor(new Color(96, 96, 192));
            g.drawString(affe2, corner + lenA1, getHeight() - corner);
            g.setColor(new Color(192, 96, 96));
            g.drawString(affe3, corner + lenA1 + lenA2, getHeight() - corner);
        }
        return corner;
    }

    public void clearArrows() {
        arrows.clear();
    }

    public void addArrow(int x1, int x2, int x3, int y1, int y2, boolean dottedArrow) {
        arrows.add(new Arrow(x1, x2, x3 - 2, y1, y2, dottedArrow));
    }

    /**
     * Set given dump block as main dump block, remove everything else.
     */
    public void setMainDumpBlock(DumpBlock dumpBlock) {
        DumpTextBox box2 = new DumpTextBox(dumpBlock);
        removeAll();
        clearArrows();
        add(box2);
        mainBox = box2;
        box2.setLocation(15, 15);
        box2.doLayout();
        revalidate();
        repaint();
    }

    public DumpTextBox getMainBox() {
        return mainBox;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        // Get current mouse position.
        Point currentMousePosition = e.getLocationOnScreen();

        // Get delta to last one.
        int dx = currentMousePosition.x - lastPressedMousePosition.x;
        int dy = currentMousePosition.y - lastPressedMousePosition.y;

        // Move all chil components.
        moveAllInnerComponents(dx, dy);

        // Store current mouse position as last one.
        lastPressedMousePosition = currentMousePosition;
    }

    private void moveAllInnerComponents(int dx, int dy) {
        // Die beiden folgenden Schleifen könnte man auch dadurch einfacher gestalten,
        // indem nur die MainBox verschoben wird und dann die Repositionierung
        // angeworfen wird. Dies wäre allerdings rechentechnisch viel aufwändiger
        // (langsamer), daher gehen wir an dieser Stelle diesen komplizierteren Weg.
        // Falls sich irgendwann noch viel mehr verschiedene Komponenten auf der
        // Oberfläche tummeln sollten, könnte man jedoch überlegen, ob der einfachere
        // Weg auch der pragmatischere wäre.

        // Sämtliche Kindelemente verschieben sich um genau dieses Delta.
        for (Component comp : getComponents()) {
            comp.setLocation(comp.getX() + dx, comp.getY() + dy);
        }

        // Sämtliche Pfeile ebenfalls.
        for (Arrow arrow : arrows) {
            arrow.move(dx, dy);
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        // Kann noch ignoriert werden.
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // Kann noch ignoriert werden.
    }

    @Override
    public void mousePressed(MouseEvent e) {
        lastPressedMousePosition = new Point(e.getLocationOnScreen());
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        lastPressedMousePosition = null;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // Can be ignored.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // Can be ignored.
    }

    @Override
    public void componentAdded(ContainerEvent e) {
        registerLabels(e.getChild());
    }

    private void registerLabels(Component child) {
        child.addMouseListener(this);
        child.addMouseMotionListener(this);
        if (child instanceof Container) {
            for (Component comp : ((Container) child).getComponents()) {
                registerLabels(comp);
            }
        }
    }

    @Override
    public void componentRemoved(ContainerEvent e) {
        unregisterLabels(e.getChild());
    }

    private void unregisterLabels(Component child) {
        child.removeMouseListener(this);
        child.removeMouseMotionListener(this);
        if (child instanceof Container) {
            for (Component comp : ((Container) child).getComponents()) {
                unregisterLabels(comp);
            }
        }
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        moveAllInnerComponents(0, e.getUnitsToScroll() * getFont().getSize() * -5);
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // Ignore this.
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
        // Open another file?
        case KeyEvent.VK_O:
            openNewFile();
            break;
        case KeyEvent.VK_DOWN:
            chooseDown();
            break;
        case KeyEvent.VK_UP:
            chooseUp();
            break;
        case KeyEvent.VK_LEFT:
            goToParentBox();
            break;
        case KeyEvent.VK_RIGHT:
            openOrGoToChildBox();
            break;
        case KeyEvent.VK_ENTER:
            if (!openOrGoToChildBox()) {
                openOrGoToChildBox();
            }
            break;
        case KeyEvent.VK_BACK_SPACE:
            if (goToParentBox()) {
                choosenAddressedLabel.closeChildrenDumpBoxes();
            }
            break;
        case KeyEvent.VK_DELETE:
            if (choosenAddressedLabel != null) {
                choosenAddressedLabel.closeChildrenDumpBoxes();
            }
            break;
        default:
            // System.out.println("pressed: " + e.getKeyCode());
        }
    }

    private void openNewFile() {
        int rc = fc.showOpenDialog(this);
        if (rc == JFileChooser.APPROVE_OPTION) {
            byte[] content = null;
            try {
                content = Files.readAllBytes(Paths.get(fc.getSelectedFile().toURI()));
                setMainDumpBlock(dataDivider.divide(content));
            } catch (IOException e) {
                JOptionPane.showMessageDialog(this, "Error on reading class file!", "File reading error",
                        JOptionPane.ERROR_MESSAGE);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, "Error on parsing java class file!", "Class parsing error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private boolean goToParentBox() {
        if (choosenAddressedLabel != null) {
            // Dump box of choosen addressed label:
            DumpTextBox choosenDumpBox = choosenAddressedLabel.getParentDumpBox();
            AddressedLabel al = choosenDumpBox.pointingAddressedLabel;
            if (al != null) {
                return al.choose();
            }
        }
        return false;
    }

    /**
     * If the child boxes are open, we dive into them (and return true). If not, we
     * open them (but return false).
     */
    private boolean openOrGoToChildBox() {
        if (choosenAddressedLabel == null) {
            return false;
        }

        List<DumpTextBox> children = choosenAddressedLabel.getChildren();

        // No children open?
        if (children == null) {
            choosenAddressedLabel.openChildrenDumpBoxes();
            return false;
        }

        // Dive into first child.
        if (!children.isEmpty()) {
            return children.get(0).chooseLastChoosenAddressedLabel();
        }

        // Not possible - there are no children.
        return false;
    }

    private void chooseDown() {
        // No choosen addressed label? Choose first one in main dump text box.
        if (choosenAddressedLabel == null) {
            mainBox.chooseFirstAddressedLabel();
            return;
        }

        // Choose the next one.
        DumpTextBox box = choosenAddressedLabel.getParentDumpBox();
        box.chooseNextAddressedLabel(choosenAddressedLabel);
    }

    private void chooseUp() {
        // No choosen addressed label? Choose last one.
        if (choosenAddressedLabel == null) {
            mainBox.chooseLastAddressedLabel();
            return;
        }

        // Choose the one before.
        DumpTextBox box = choosenAddressedLabel.getParentDumpBox();
        box.choosePreviousAddressedLabel(choosenAddressedLabel);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // Ignore this.
    }

    /**
     * An addressed label is the current one (kind of "focus", but not the real
     * swing focus because this FidPanel has currently the focus (or not)).
     * @return <i>true</i> if the given addressed label is set (and has been
     * changed!).
     */
    public boolean setChoosenAddressedLabel(AddressedLabel label) {
        // The same? Forget it.
        if (label == choosenAddressedLabel) {
            return false;
        }

        // Remove this state from old label.
        if (choosenAddressedLabel != null) {
            choosenAddressedLabel.unchoose();
        }

        // Set new label.
        choosenAddressedLabel = label;
        label.choose();

        return true;
    }

    public AddressedLabel getChoosenAddressedLabel() {
        return choosenAddressedLabel;
    }

    public void moveAllChildren(int dx, int dy) {
        // Move all boxes.
        for (Component child : getComponents()) {
            if (child instanceof DumpTextBox) {
                DumpTextBox box = (DumpTextBox) child;
                box.setLocation(box.getX() + dx, box.getY() + dy);
            }
        }

        // Move all arrows.
        for (Arrow arrow : arrows) {
            arrow.x1 += dx;
            arrow.x2 += dx;
            arrow.x3 += dx;
            arrow.y1 += dy;
            arrow.y2 += dy;
        }
    }
}
