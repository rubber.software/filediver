package com.packenius.fid.gui;

import javax.swing.JFrame;

import com.packenius.dumpapi.DumpBlock;

public class FileDiverFrame extends JFrame {
    private static final long serialVersionUID = 3914187263026754876L;

    public final FidPanel fidPanel;

    public FileDiverFrame(DumpBlock mainDumpBlock) {
        this(mainDumpBlock, true);
    }

    public FileDiverFrame(DumpBlock mainDumpBlock, boolean fullScreen) {
        if (fullScreen) {
            setUndecorated(true);
        }
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        fidPanel = new FidPanel(mainDumpBlock);
        add(fidPanel);

        setVisible(true);
    }
}
