package com.packenius.fid.gui.label;

public interface DumpLabel {
    /**
     * Returns the length of the text line in characters.
     */
    int getTextLength();

    /**
     * Set the max character count on this line.
     */
    void setTextCharMax(int maxChars);
}
