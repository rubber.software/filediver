package com.packenius.fid.gui.label;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.packenius.dumpapi.CrossPointer;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.MainDumpBlock;
import com.packenius.fid.gui.DumpTextBox;
import com.packenius.fid.gui.FidPanel;

/**
 * Text line with hex address on the left side.
 * @author Christian Packenius, 2019.
 */
public class AddressedLabel extends JPanel implements MouseListener, DumpLabel {
    private static final long serialVersionUID = 6479034758271440441L;
    private static final Color textColor = new Color(32, 32, 32);
    private static final Color adressColor = new Color(0, 0, 96);
    private static final Color crossColor = new Color(96, 0, 128);
    private static final Color enteredColor = new Color((textColor.getRGB() | 0xff000000) ^ 0xffffff);

    /**
     * The dump block (bytes!) that is represented by this addressed label line.
     */
    private final DumpBlock subBlock;

    /**
     * If this addressed label line points to another dump text box (with an arrow),
     * this List exists and has usually only one entry. If a cross block is set,
     * than there is a second entry in it.
     */
    private List<DumpTextBox> children = null;

    /**
     * This AddressedLabel is a JPanel that consists of three different labels.
     */
    private JLabel labelAdress, labelText, labelCross;

    /**
     * A cross block is a block that is no real child but a logical child of this
     * dump block because there is any kind of pointer within this dump block part.
     */
    private final DumpBlock crossBlock;

    /**
     * Common addressed text line that points to another sub block.
     */
    public AddressedLabel(DumpBlock subBlock, DumpTextBox parent) {
        super(new BorderLayout());

        this.subBlock = subBlock;

        MainDumpBlock mainBlock = subBlock.reader.getMainBlock();
        CrossPointer crossPointer = subBlock.getCrossPointer();
        crossBlock = mainBlock.getCrossPointerDumpBlocks(crossPointer);

        String headline = subBlock.toString();
        long adress = subBlock.getStartAddress();

        init(parent, headline, adress);
    }

    /**
     * An addressed line within a leaf, shows the real hex dump of this bytes part.
     */
    public AddressedLabel(long adress, String headline, DumpTextBox parent) {
        super(new BorderLayout());

        this.subBlock = null;
        this.crossBlock = null;

        init(parent, headline, adress);
    }

    private void init(DumpTextBox parent, String headline, long adress) {
        setFont(parent.getFont());
        createInnerLabels(adress, headline);
        unchoose();
        parent.add(this);
    }

    private void createInnerLabels(long adress, String headline) {
        labelAdress = createLabel(DumpTextBox.adressString(adress) + " ", adressColor);
        labelText = createLabel(headline, textColor);
        add(labelAdress, BorderLayout.WEST);
        add(labelText, BorderLayout.CENTER);
        if (crossBlock != null) {
            labelCross = createLabel(" (X)", crossColor);
            add(labelCross, BorderLayout.EAST);
        }
    }

    private JLabel createLabel(String text, Color color) {
        JLabel label = new JLabel(text);
        label.setFont(getFont());
        label.addMouseListener(this);
        return label;
    }

    public void unchoose() {
        labelAdress.setForeground(adressColor);
        labelText.setForeground(textColor);
        if (labelCross != null) {
            labelCross.setForeground(crossColor);
        }
        setBackground(null);
        setOpaque(false);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (subBlock != null) {
            // Make child view invisible?
            if (children != null) {
                closeChildrenDumpBoxes();
            }

            // Or re-open it?
            else {
                openChildrenDumpBoxes();
            }
        }

        // This clicked addressed label is the choosen one.
        choose();
    }

    public void closeChildrenDumpBoxes() {
        if (children != null) {
            for (DumpTextBox child : children) {
                child.destroy();
            }
            children = null;

            // Reposition everything.
            getParentDumpBox().rePositionRekursive();
        }
    }

    /**
     * The child dump boxes that belongs to this label are closed (maybe have never
     * been opened). Open it here.
     */
    public void openChildrenDumpBoxes() {
        // Is this addressed label part of a leaf dump text box?
        if (subBlock == null) {
            return;
        }

        children = new ArrayList<DumpTextBox>();

        DumpTextBox parentBox = getParentDumpBox();
        FidPanel fidPanel = parentBox.getParentFidPanel();

        // Create the (graphical) child dump text box.
        DumpTextBox newBox = new DumpTextBox(subBlock, this);
        children.add(newBox);
        fidPanel.add(newBox);

        // If there is a cross pointer, show it in a special block.
        if (crossBlock != null) {
            DumpTextBox crossBox = new DumpTextBox(crossBlock, this, true);
            children.add(crossBox);
            fidPanel.add(crossBox);
        }

        fidPanel.getParent().revalidate();

        // Reposition everything.
        parentBox.rePositionRekursive();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // Ignorieren, ist noch nicht relevant.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // Ignorieren, ist noch nicht relevant.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // Ignore mouse entering.
        // choose();
    }

    /**
     * Mark this label with extra color, if it is chosen.
     */
    public boolean choose() {
        setChoosenColor();
        return getParentDumpBox().setCurrentAddressedLabel(this);
    }

    /**
     * Returns the dump text box that this label is part of.
     */
    public DumpTextBox getParentDumpBox() {
        return (DumpTextBox) getParent();
    }

    private void setChoosenColor() {
        labelAdress.setForeground(enteredColor);
        labelText.setForeground(enteredColor);
        if (labelCross != null) {
            labelCross.setForeground(enteredColor);
        }
        setBackground(textColor);
        setOpaque(true);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // Ignore mouse exit.
        // unchoose();
    }

    @Override
    public int getTextLength() {
        int crossLength = labelCross == null ? 0 : labelCross.getText().length();
        return labelAdress.getText().length() + labelText.getText().length() + crossLength;
    }

    @Override
    public void setTextCharMax(int maxChars) {
        // Create additional spaces for showing the text left aligned.
        String text = labelText.getText();
        int addCount = maxChars - labelAdress.getText().length() - text.length();
        if (labelCross != null) {
            addCount -= labelCross.getText().length();
        }
        labelText.setText(text + LineLabel.repeatChar(addCount, ' '));
    }

    public List<DumpTextBox> getChildren() {
        return children;
    }

    public boolean isChoosen() {
        return this == getParentDumpBox().getParentFidPanel().getChoosenAddressedLabel();
    }
}
