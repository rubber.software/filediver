package com.packenius.fid.examples;

import com.packenius.datadivider.DataDivider;
import com.packenius.dumpapi.MainDumpBlock;
import com.packenius.fid.gui.FileDiverFrame;

/**
 * Example for how to start FiD.
 * @author Christian Packenius, 2019.
 */
public class FileDiverRunnableClass {
    public static void main(String[] args) {
        byte[] content = ByteContentUtils.getClassBytes(Runnable.class);
        MainDumpBlock mainDumpBlock = new DataDivider().divide(content);
        new FileDiverFrame(mainDumpBlock);
    }
}
