package com.packenius.fid.examples.dynamiccompilation;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

/**
 * Compiles a java class full in-memory. Has possibility to instantiate or run
 * it after that.
 * @author Christian Packenius, 2019.
 */
public class DynamicCompiler {
    public final InMemoryJavaSourceCode imSources;

    private ClassFileManager fileManager;

    public final boolean compilationOkay;

    /**
     * Compiles dynamic generated source code.
     */
    public DynamicCompiler(String className, String sourceCode) {
        // Init.
        imSources = new InMemoryJavaSourceCode(className, sourceCode);
        JavaFileObject javaFileObjects[] = new JavaFileObject[] { imSources };
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager standardFileManager = compiler.getStandardFileManager(null, null, null);
        fileManager = new ClassFileManager(standardFileManager, imSources);
        List<? extends JavaFileObject> compilationUnits = Arrays.asList(javaFileObjects);
        List<String> compileOptions = new ArrayList<>();
        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();

        // Compilation.
        CompilationTask compilerTask = compiler.getTask(null, fileManager, diagnostics, compileOptions, null,
                compilationUnits);
        compilationOkay = compilerTask.call();
    }

    /**
     * Instantiates the successfully translated class in an own thread and starts
     * the main class.
     */
    public void runCompiledClassInOwnThread(int timeLimit) {
        ExecutorService service = Executors.newSingleThreadExecutor();

        try {
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    try {
                        fileManager.getClassLoader(null).loadClass(imSources.name)
                                .getDeclaredMethod("main", new Class[] { String[].class })
                                .invoke(null, new Object[] { null });
                    } catch (ClassNotFoundException e) {
                        System.err.println("Class not found: " + e);
                    } catch (NoSuchMethodException e) {
                        System.err.println("No such method: " + e);
                    } catch (IllegalAccessException e) {
                        System.err.println("Illegal access: " + e);
                    } catch (InvocationTargetException e) {
                        System.err.println("RuntimeError: " + e.getTargetException());
                    }
                    try {
                        imSources.delete();
                        fileManager.close();

                        // Useless or not?!
                        ResourceBundle.clearCache(ClassLoader.getSystemClassLoader());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };

            Future<?> f = service.submit(r);

            if (timeLimit > 0) {
                f.get(timeLimit, TimeUnit.MILLISECONDS);
            } else {
                // Near Infinity...
                f.get(Integer.MAX_VALUE / 2, TimeUnit.DAYS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            service.shutdown();
        }
    }

    public Object newInstance(Class<?>... parameterTypes) throws Exception {
        return fileManager.getClassLoader(null).loadClass(imSources.name).getDeclaredConstructor(parameterTypes)
                .newInstance();
    }
}
