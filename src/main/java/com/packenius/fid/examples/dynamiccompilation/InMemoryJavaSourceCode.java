package com.packenius.fid.examples.dynamiccompilation;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

import javax.tools.SimpleJavaFileObject;

/**
 * Holds name, source and java .class content of a java class.
 * @author Christian Packenius, 2019.
 */
public class InMemoryJavaSourceCode extends SimpleJavaFileObject {
    public final String name;

    public final String sourceCode;

    /**
     * Class file content will be stored in this output stream.
     */
    private ByteArrayOutputStream baos = new ByteArrayOutputStream();

    protected InMemoryJavaSourceCode(String name, String source) {
        super(URI.create("string:///" + name.replace('.', '/') + Kind.SOURCE.extension), Kind.SOURCE);
        this.name = name;
        this.sourceCode = source;
    }

    @Override
    public CharSequence getCharContent(boolean ignoreEncErrors) throws IOException {
        return sourceCode;
    }

    /**
     * Will be used by our file manager to get the byte code that can be put into
     * memory to instantiate our class
     * @return compiled byte code
     */
    public byte[] getBytes() {
        return baos.toByteArray();
    }

    /**
     * Will provide the compiler with an output stream that leads to our byte array.
     * This way the compiler will write everything into the byte array that we will
     * instantiate later
     */
    @Override
    public OutputStream openOutputStream() throws IOException {
        return baos;
    }
}
