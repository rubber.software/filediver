package com.packenius.fid.examples;

import com.packenius.datadivider.DataDivider;
import com.packenius.dumpapi.MainDumpBlock;
import com.packenius.fid.gui.FileDiverFrame;

import hello.world.HelloWorld;

/**
 * Example for how to start FiD.
 * @author Christian Packenius, 2019.
 */
public class FileDiverHelloWorldClass {
    public static void main(String[] args) {
        byte[] content = ByteContentUtils.getClassBytes(HelloWorld.class);
        MainDumpBlock mainDumpBlock = new DataDivider().divide(content);
        new FileDiverFrame(mainDumpBlock);
    }
}
