package com.packenius.fid.examples;

import java.awt.BorderLayout;
import java.awt.Font;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.packenius.datadivider.DataDivider;
import com.packenius.datadivider.javaclass.MethodDescription;
import com.packenius.datadivider.javaclass.Methods;
import com.packenius.datadivider.javaclass.attr.AttributeDescription;
import com.packenius.dumpapi.DumpBlock;
import com.packenius.dumpapi.MainDumpBlock;
import com.packenius.fid.examples.dynamiccompilation.DynamicCompiler;
import com.packenius.fid.gui.FidPanel;

/**
 * Example for how to start FiD to edit a Java snippet and watch the Byte Code
 * changing online.
 * @author Christian Packenius, 2019.
 */
public class FileDiverJavaSnippet extends JFrame implements DocumentListener {
    private static final long serialVersionUID = -5781918900508316907L;

    private static String initialSnippetSource = "System.out.println(\"Hello FiD!\");";

    private volatile String className;

    private final FidPanel fidPanel;

    private final JTextArea text;

    private AttributeDescription getSnippetCode(MainDumpBlock mainDumpBlock) {
        // Get the Methods blocks (there is exactly one in every java class file).
        List<DumpBlock> methodsDB = mainDumpBlock.reader.getSubBlocks(mainDumpBlock, Methods.class);
        Methods methods = (Methods) methodsDB.get(0);

        // Get snippet() method.
        MethodDescription snippet = null;
        for (MethodDescription method : methods.methods) {
            if (method.methodName.equals("snippet")) {
                snippet = method;
            }
        }

        // Get code attribute of the snippet method.
        List<AttributeDescription> attributes = snippet.attributes.attributes;
        AttributeDescription code = null;
        for (AttributeDescription attributeDescription : attributes) {
            if ("Code".equals(attributeDescription.getName())) {
                code = attributeDescription;
            }
        }
        return code;
    }

    private String createSnippetClassSource(String snippetSource) {
        String simpleClassName = "Snippet" + System.nanoTime();
        className = "wumpel.putz." + simpleClassName;
        StringWriter writer = new StringWriter();
        PrintWriter out = new PrintWriter(writer);
        out.println("package wumpel.putz;");
        out.println("public class " + simpleClassName + " {");
        out.println("  public static void snippet() {");
        out.println(snippetSource);
        out.println("  }");
        out.println("}");
        out.close();
        String source = writer.toString();
        return source;
    }

    private byte[] getSnippetClassFileContent(String source) {
        DynamicCompiler dynCompiler = new DynamicCompiler(className, source);
        if (!dynCompiler.compilationOkay) {
            return null;
        }
        byte[] content = dynCompiler.imSources.getBytes();
        return content;
    }

    public FileDiverJavaSnippet() {
        super("Just in time compilation of Java code snippet");
        String source = createSnippetClassSource(initialSnippetSource);
        byte[] content = getSnippetClassFileContent(source);

        MainDumpBlock mainDumpBlock = new DataDivider().divide(content);
        AttributeDescription code = getSnippetCode(mainDumpBlock);

        setLayout(new BorderLayout(5, 5));
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        fidPanel = new FidPanel(code);
        text = new JTextArea(initialSnippetSource, 5, 5);
        text.setFont(new Font("Consolas", Font.PLAIN, 18));
        text.getDocument().addDocumentListener(this);
        add(fidPanel, BorderLayout.CENTER);
        add(text, BorderLayout.SOUTH);
        setVisible(true);
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        snippetSourceChange();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        snippetSourceChange();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        snippetSourceChange();
    }

    private void snippetSourceChange() {
        final String snippetText = text.getText();
        String source = createSnippetClassSource(snippetText);
        final String myClassName = className;
        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
        try {
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    try {
                        // If there is another (newer) class to compile, don't use this one.
                        if (!myClassName.equals(className)) {
                            return;
                        }

                        // Compile snippet class.
                        byte[] content = getSnippetClassFileContent(source);

                        // Use result if exists and is current one.
                        if (content != null && myClassName.equals(className)) {
                            MainDumpBlock mainDumpBlock = new DataDivider().divide(content);
                            AttributeDescription code = getSnippetCode(mainDumpBlock);
                            SwingUtilities.invokeAndWait(new Runnable() {
                                @Override
                                public void run() {
                                    // Only show newest result!
                                    if (myClassName.equals(className)) {
                                        fidPanel.setMainDumpBlock(code);
                                    }
                                }
                            });
                        }
                    } catch (Exception e) {
                        // Don't set code attribute into FidPanel.
                    }
                }
            };

            singleThreadExecutor.submit(r);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            singleThreadExecutor.shutdown();
        }
    }

    public static void main(String[] args) throws Exception {
        new FileDiverJavaSnippet();
    }
}
