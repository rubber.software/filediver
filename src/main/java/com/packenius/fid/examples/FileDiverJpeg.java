package com.packenius.fid.examples;

import java.io.IOException;
import java.io.InputStream;

import com.packenius.datadivider.DataDivider;
import com.packenius.dumpapi.MainDumpBlock;
import com.packenius.fid.gui.FileDiverFrame;

/**
 * Example for how to start FiD.
 * @author Christian Packenius, 2019.
 */
public class FileDiverJpeg {
    public static void main(String[] args) throws IOException {
        try (InputStream in = FileDiverJpeg.class.getResourceAsStream("pexels-photo-1860319.jpeg")) {
            byte[] content = ByteContentUtils.getInputStreamBytes(in);
            MainDumpBlock mainDumpBlock = new DataDivider().divide(content);
            new FileDiverFrame(mainDumpBlock);
        }
    }
}
