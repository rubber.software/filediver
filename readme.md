FiD - The File Diver
====================

A GUI for diving into file content.

With FiD you can dive into different file types and view them at byte level.
For this the content of the viewed file is subdivided into blocks. Each block's
content is described.

Speech
------

"FiD" is spoken as "fight".

Maven Dependency
----------------

    <dependency>
        <groupId>com.packenius</groupId>
        <artifactId>com.packenius.filediver</artifactId>
        <version>0.9</version>
    </dependency>

Creating application
--------------------

For using FiD you need three steps to dive into a file:

1. Read the file content.
2. Divide the file into blocks using a DataDivider instance.
3. Create a FileDiverFrame for starting FiD.

Try this small application for diving into the Java Runnable class:

    public static void main(String[] args) {
        byte[] content = Statics.getClassBytes(Runnable.class);
        MainDumpBlock mainDumpBlock = new DataDivider().divide(content);
        new FileDiverFrame(mainDumpBlock);
    }

This will open a full screen new window with a diving GUI for the given class.

Another way is to integrate FiD into your Java Swing application. Just create
a FidPanel (which extends a JPanel) with the main dump block and add it into
your GUI:

    add(new FidPanel(mainDumpBlock));

You can change the content to dive into by setting another main dump block:

    byte[] content = {...any file content or byte array you want...};
    setMainDumpBlock(dataDivider.divide(content));

Examples
--------

Look into package "com.packenius.fid.examples" for some examples:

- FileDiverHelloKotlin

  Opens a Kotlin example class file.
  
- FileDiverHelloWorldClass

  Opens an own class in the classpath.
  
- FileDiverJavaSnippet

  Cool thing: Edit a Java snippet online and edit it while
  watching the Code Attribute changing.
  
- FileDiverRunnableClass

  How to dive into an JRE class (Runnable interface here). 

Control with mouse
------------------

Just klick on an addressed line to open or close the child block. Shift the
full area with pressed mouse button.

Control with keyboard
---------------------

Use _up and down arrows_ to move within a block or its siblings.

Press _right arrow_ to open a closed child block or to jump into an opened
child block. You can open and jump into a child block with _enter_.

_Backspace_ closes the current block, _delete_ closes the opened child
block.

With key 'O' open another file in the same FidPanel.

